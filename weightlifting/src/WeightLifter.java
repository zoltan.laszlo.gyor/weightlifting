public class WeightLifter {
    private String name;
    private int weight;
    public static WeightLifter strongestWeigthlifter = null;

    private WeightLifter(String name, int weight){
        this.name = name;
        this.weight = weight;
        if (strongestWeigthlifter == null){
            strongestWeigthlifter = this;
        }else if(this.weight > strongestWeigthlifter.getWeight()){
            strongestWeigthlifter = this;
        }
    }

    public static WeightLifter make(String name, int weight){
        if (name.length() < 2){
            return null;
        }
        for (int i = 0; i < name.length(); i++) {
            if (Character.isLetter(name.charAt(i)) == false){
                return null;
            }
        }
        if(weight <1 || weight > 300){
            return null;
        }
        return new WeightLifter(name, weight);
    }

    public int getWeight(){
        return this.weight;
    }

    public boolean strongerThan(int weight){
        if (this.weight > weight){
            return true;
        }
        return false;
    }

    public String show(){ //jobbra igazítás?
        String spacesNeeded = "";
        if (Integer.toString(this.weight).length() == 1){
            spacesNeeded = "  ";
        }else if(Integer.toString(this.weight).length() == 2){
            spacesNeeded = " ";
        }

        return this.name + " - " + spacesNeeded + " kg";
    }

    public static WeightLifter getStrongestWeigthlifter() {
        return strongestWeigthlifter;
    }
}
